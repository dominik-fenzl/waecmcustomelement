const fs = require('fs-extra');
const concat = require('concat');
(async function build() {
  const files = [
    './dist/waecm-custom-element/runtime-es2015.js',
    './dist/waecm-custom-element/polyfills-es2015.js',
    './dist/waecm-custom-element/scripts.js',
    './dist/waecm-custom-element/main-es2015.js',
  ]
  await fs.ensureDir('elements')
  await concat(files, 'elements/cookie-consent-banner.js');
  // await fs.copyFile('./dist/waecm-custom-element/styles.css', 'elements/styles.css');
})()

import {BrowserModule} from '@angular/platform-browser';
import {Injector, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {createCustomElement} from '@angular/elements';
import {CookieConsentBannerComponent} from './cookie-consent-banner/cookie-consent-banner.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    CookieConsentBannerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  entryComponents: [CookieConsentBannerComponent],
  // bootstrap: [AppComponent] // => use for local development localhost:4200
})

export class AppModule {
  constructor(private injector: Injector) {
    const el = createCustomElement(CookieConsentBannerComponent, {injector});
    customElements.define('cookie-consent-banner', el);
  }

  ngDoBootstrap() {
  }
}

import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-cookie-consent-banner',
  templateUrl: './cookie-consent-banner.component.html',
  styleUrls: ['./cookie-consent-banner.component.css'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class CookieConsentBannerComponent implements OnInit {
  @ViewChild('alert', { static: true }) alert: ElementRef;
  @Input() public applicationName: string;
  @Input() public policyLink: string;
  @Output('on-accept') cookiesAccepted = new EventEmitter();
  public bannerHidden = false;
  locationChanged: boolean = false;

  constructor(private meta: Meta) {
    this.meta.addTags(
      [{ name: 'viewport', content: 'width=device-width, initial-scale=1.0' }],
      true);
  }

  ngOnInit(): void {
    if(localStorage.getItem('cookieAccepted')) this.bannerHidden = true;
  }

  ngAfterViewChecked() {
    this.locationChanged = true;
  }

  onAccept(): void {
    localStorage.setItem('cookieAccepted', 'true');
    this.bannerHidden = true;
    this.cookiesAccepted.emit(true);
  }


  clearLocalStorage(): void {
    localStorage.clear();
  }

}
